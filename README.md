[![build status](https://gitlab.com/miro_ilias_small_projects/newton-raphson/badges/master/build.svg)](https://gitlab.com/miro_ilias_small_projects/newton-raphson/builds)
[![build status](https://magnum-ci.com/status/61add09ad2108561ff16dab83b3e0484.png)](https://magnum-ci.com/projects/4293)

Newton-Raphson procedure
========================

Simple Fortran90 program showing Newton-Raphson numerical solution of two nonlinear equations.

How to compile and run:
-----------------------

From the main directory launch, the setup command (see python setup --help):

```
 python setup --fc=gfortran build_gf
 cd build_gf
 make
```


Get the first set of roots:

```
 src/newton-raphson.x -10 10
```

Calculate the second set of roots:

```
 src/newton-raphson.x 10  10
```

Upon happy finishing you will see *PASSED* word in the output, otherwise you get program STOP.

This project buildup is based solely on the [*autocmake* project](https://github.com/scisoft/autocmake).
