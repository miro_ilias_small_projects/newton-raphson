 SUBROUTINE RESULTS(Pstart,P0,P1,F1,Cond,K,MaxIter)
   INTEGER :: Cond,K,MaxIter
   Real*8 :: F1(1:2),Pstart(1:2),P0(1:2),P1(1:2)
   WRITE(*,*)' '
   WRITE(*,*)'------------------------------------------------------------------ '
   WRITE(*,*)'Newton-Raphson iteration was used to solve the non-linear system'
   CALL PRINTFUN
   WRITE(*,*)'starting with X0 =',Pstart(1),' and Y0 =',Pstart(2)
   WRITE(*,*) 'It took ',K,' iterations to compute the solution point (X,Y).'
   WRITE(*,*)' '
   IF (Cond.EQ.0) THEN
      WRITE(*,*)'However, the maximum number of iterations -',MaxIter,'- was exceeded !'
   ELSEIF (Cond.EQ.1) THEN
      WRITE(*,*)'However, division by zero was encountered !'
   ELSEIF (Cond.EQ.2) THEN
      WRITE(*,*)'The converged solution is within the desired tolerances.'
   ENDIF
      WRITE(*,*)' '
   IF (Cond.EQ.0.OR.Cond.EQ.1) THEN
      WRITE(*,*)'         X(',K,') =',P1(1),'  ,  Y(',K,') =',P1(2)
   ELSEIF (Cond.EQ.2) THEN
     WRITE(*,*)'The Roots are  X  =',P1(1),', Y  =',P1(2)
   ENDIF
   WRITE(*,*)'Differences between last two iterated roots:'
   WRITE(*,*)'     |DX| =',ABS(P1(1)-P0(1)),'  ,   |DY| =',ABS(P1(2)-P0(2))
   WRITE(*,*)'For control, function values of obtained roots are: '
   WRITE(*,*)'     F1 (X,Y) =',F1(1)
   WRITE(*,*)'     F2 (X,Y) =',F1(2)
   IF (Cond.EQ.2) THEN
      WRITE(*,*)'PASSED'
   ELSE 
      STOP 1
   ENDIF
 END SUBROUTINE
